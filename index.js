#!/usr/bin/env node

const yargs = require('yargs');
const Fibonacci = require('./fibonacci.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <enesimo>")
   .option("n", { alias: "enesimo", describe: "n-esimo numero da seq Fibonacci", type: "string", demandOption: true })
   .argv;

  const fib = new Fibonacci(options.n);
    
  console.log(fib.myFib());
}
