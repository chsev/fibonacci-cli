import Fibonacci from "./fibonacci";

describe('Fibonacci Sequence', () => {
  describe('Tests Fib()', () => {

    test('Fib(1) == 1', () => {
      const fibonacci = new Fibonacci(1);
      expect(fibonacci.myFib()).toEqual(1);
    });

    test('Fib(5) == 5', () => {
      const fibonacci = new Fibonacci(5);
      expect(fibonacci.myFib()).toEqual(5);
    });

    test('Fib(6) == 8', () => {
      const fibonacci = new Fibonacci(6);
      expect(fibonacci.myFib()).toEqual(8);
    });

    test('Fib(40) == 102334155', () => {
      const fibonacci = new Fibonacci(40);
      expect(fibonacci.myFib()).toEqual(102334155);
    });

  })
})
