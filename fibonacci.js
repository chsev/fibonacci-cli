class Fibonacci{
  constructor(enesimo){
    this.enesimo = enesimo;
    this.result = this.#myFib(enesimo);
  }

  myFib(){
    return(this.result);
  }

  #myFib( n ){
    if( n == 0 || n==1 ){
      return n;
    }
    return this.#myFib(n-1)+this.#myFib(n-2);
  }
}

module.exports = Fibonacci;